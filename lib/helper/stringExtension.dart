import 'tran.dart' as _t;
extension StringExtension on String {
  String toFirstUppercase() {
    try{
      return "${this[0].toUpperCase()}${this.substring(1)}";
    } catch (_){
      return this;
    }
  }
  String toFirstUppercaseWord() {
    try{
      String stringWord = '';
      List<String> stringList = this.split(' ');
      if(stringList == null ? false : (stringList.length > 0)){
        stringList.forEach((v) {
          stringWord = "${stringWord == '' ? stringWord : stringWord + ' '}" + "${v[0].toUpperCase()}${v.substring(1)}";
        });
        return stringWord;
      }
      return "${this[0].toUpperCase()}${this.substring(1)}";
    } catch (_){
      return this;
    }

  }
  String translate() {
    return _t.tran(this);
  }
}