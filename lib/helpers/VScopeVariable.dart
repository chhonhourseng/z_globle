import 'package:flutter/material.dart';

class VScopeVariable {
  String id;
  dynamic coSetState;
  BuildContext coContext;

  void refreshState(){
    if(coSetState != null){
      coSetState(() {
      });
    }
  }

  void initialize({@required dynamic setState, @required BuildContext context, String id}){
    this.coSetState = setState;
    this.coContext = context;
    this.id = id;
    this.firstState();
  }

  void firstState(){

  }
}