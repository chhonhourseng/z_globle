import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dataRow.dart';
import '../g.dart' as gCo;
import '../g_widget.dart' as gWidget;
import '../g_form.dart' as gForm;
import '../helper/stringExtension.dart';

class RenderRowData{

  String url = '';
  String q = '';
  String where = '';
  bool showSomeThingWrong = false;
  bool autoGetData = true;
  CoDataRow dataRow = new CoDataRow();
  Map<String, dynamic> field = {};

  var coSearchTextController = TextEditingController();
  var _coSearchSubTextController = TextEditingController();

  Future getData(setState, {bool refresh : false, Map<String, dynamic> fields})async{
    String coUrl = await getURL(this.url);
    this.url = coUrl;
    String coQ = await getQ(this.q);
    coQ = coQ != '' ? '&q=${coQ}' : '';
    String coWhere = await getWhere(this.where);
    if(fields != null){
      this.field.addAll(fields);
    }
    Map<String, dynamic> coFields = await getFields(this.field);
    bool coShowSomeThingWrong = await getShowSomeThingWrong(this.showSomeThingWrong);
    //print('=========URL========${coUrl}');
    await this.dataRow.getDataRowSetState(
        setState,
        url: coUrl,
        refresh: refresh,
        where: '${coQ}${coWhere}',
        showSomeThingWrong: coShowSomeThingWrong,
        fields: coFields,
        prepareData:(val) async {
          return await prepareDataDropDownDataRows(val);
        }
    );
  }
  Future processGetData(setState)async{
    await getData(setState, refresh: true).then((value){
      return value;
    });
  }
  Future<String> getURL(oldVal)async{
    return oldVal;
  }
  Future<String> getQ(oldVal)async{
    return oldVal;
  }
  Future<String> getWhere(oldVal)async{
    return oldVal;
  }
  Future<Map<String, dynamic>> getFields (oldVal)async{
    return oldVal;
  }
  Future<bool> getShowSomeThingWrong(oldVal)async{
    return oldVal;
  }
  prepareDataDropDownDataRows(rowList)async{
    Map row = {
      'list' : await dataRowsToDropdownData(dataList: rowList['rows_branch']['data']),
      'total' : rowList['rows_branch']['total'],
    };
    return row;
  }
  dataRowsToDropdownData({List dataList}){
    if(dataList != null){
      List _list= [];
      dataList.forEach((v) {
        _list.add({
          'id' : v['id'],
          'name' : v['title'],
        });
      });
      return _list;
    }
    return null;
  }
/// =========================================================================
/// STATE MANAGEMENT
  String id;
  dynamic coSetState;
  BuildContext coContext;

  ScrollController controller;

  void refreshState(){
    if(coSetState != null){
      coSetState(() {
      });
    }
  }

  void initialize({@required dynamic setState, @required BuildContext context, String id}){
    this.coSetState = setState;
    this.coContext = context;
    this.id = id;
    this.firstState();
  }

  coScrollListener() {
    gCo.scrollRenderDataController(this.controller, RenderRowData: this).then((value){
      this.getMoreData(refresh: false);
    });
  }

  void firstState(){
    this.controller = ScrollController();
    this.controller.addListener(coScrollListener);
    if(this.autoGetData){
      this.getMoreData();
    }
  }

  Future getMoreData({refresh: true}) async {
    if(refresh && this.dataRow.list != null){
      if(this.dataRow.list.length > 10){
        gCo.scrollToTop(this.controller);
      }
    }
    this.getData(this.coSetState, refresh: refresh).then((res){
      /// printDebugScope
      //gCo.printDebugScope(output: json.encode(coRenderRowData.dataRow.list), note: coRenderRowData.url, fields: fields);
    });
  }
  Future<void> onRefresh() {
    return this.getMoreData(refresh: true);
  }

  void searchAction(){
    gWidget.coModalBottomSheet(this.coContext,
        build: this.searchBuildWidget(),
        text: 'search'.translate().toFirstUppercaseWord(),
        backgroundColor: Colors.grey[100],
        fullScreen: true,
        withOK: true,
        onOK: (){
          Navigator.of(this.coContext, rootNavigator: true).pop();
          this.coSearchTextController = TextEditingController(text: this._coSearchSubTextController.text);
          this.refreshState();
          this.getMoreData();
        }
    );
  }
  Widget searchBuildWidget() {
    this._coSearchSubTextController = TextEditingController(text: this.coSearchTextController.text);
    return StatefulBuilder(
        builder: (BuildContext context2, StateSetter setState2) {
        return new Container(
            width: MediaQuery.of(this.coContext).size.width ,
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 8.0),
                Padding(
                    padding: EdgeInsets.only(left: 10.0, right: 10.0),
                    child: gForm.coTextField(
                      controller: this._coSearchSubTextController,
                      text: 'search'.translate().toFirstUppercaseWord() + '....',
                      textInputAction: TextInputAction.search,
                      fillColor: Colors.white,
                      suffixIcon: Container(
                        width: this._coSearchSubTextController.text != '' ? 98 : 50,
                        child: Row(
                          children: [
                            this._coSearchSubTextController.text != '' ? IconButton(
                                icon: Icon(Icons.clear, color: Colors.black54,),
                                onPressed: () {
                                  this._coSearchSubTextController = TextEditingController(text: '');
                                  setState2(() {
                                  });
                                }) : gWidget.coZero(),
                            IconButton(
                                icon: Icon(Icons.search),
                                onPressed: () {
                                  Navigator.of(this.coContext, rootNavigator: true).pop();
                                  this.coSearchTextController = TextEditingController(text: this._coSearchSubTextController.text);
                                  this.refreshState();
                                  this.getMoreData();
                                }),
                          ],
                        ),
                      ),
                        onFieldSubmitted: (val){
                          Navigator.of(this.coContext, rootNavigator: true).pop();
                          this.coSearchTextController = TextEditingController(text: this._coSearchSubTextController.text);
                          this.refreshState();
                          this.getMoreData();
                        },
                        onChanged: (v){
                          this.refreshState();
                          setState2(() {
                          });
                        }
                    )
                ),
              ],
            ));
      }
    );
  }
}