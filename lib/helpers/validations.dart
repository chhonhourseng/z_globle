class Validations {
  String validateRequired(String value, {name: 'This Field'}) {
    if (value.isEmpty) return '${name} is required.';
    return null;
  }
  String validateDropdownDataRowRequired(String value, {name: 'This Field'}) {
    //if (value.isEmpty) return '${name} is required.';
    if (value == null || value == 'null') return '${name} is required.';
    return null;
  }
  String validateNumber(String value, {name: 'This Field'}){
    if(validateRequired(value) != null){
      return validateRequired(value, name: name);
    }
    String patttern = r'(^[0-9.]*$)';
    RegExp regExp = new RegExp(patttern);
    if (!regExp.hasMatch(value)) {
      return "${name} is Number Only";
    }
    return null;
  }
  String validateName(String value) {
    if (value.isEmpty) return 'Name is required.';
    final RegExp nameExp = new RegExp(r'^[A-za-z ]+$');
    if (!nameExp.hasMatch(value))
      return 'Please enter only alphabetical characters.';
    return null;
  }
  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  String validatePassword(String value) {
    if (value.isEmpty) return 'Please choose a password.';
    return null;
  }
  String validateMatch(String value1, {String value2, String name : 'This Field'}) {
    if (value1.isEmpty) return '${name} is required..';
    if (value1 != value2) return '${name} is not Match..';
    return null;
  }

  String validateMobile(String value) {
    if (value.length <= 9)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

//  String validateAmount(String value, String minValue, String maxValue, String currencySymbol) {
//    String patttern = r'(^[0-9.]*$)';
//    RegExp regExp = new RegExp(patttern);
//    if(value.length == 0) return '${_t.tran('please_fill_your_amount_of_money')}';
//
//    if (!regExp.hasMatch(value)) {
//      return "${_t.tran('allow_number_only')}";
//    }
//    if (double.tryParse(value) >= double.tryParse(minValue) && double.tryParse(value) <= double.tryParse(maxValue)) return null;
//    //if(1==1) return 'Amount Of Money must be of 100\$ to 5000\$';
//    return '${_t.tran('amount_Of_money_must_be_of')} ${minValue}${currencySymbol} ${_t.tran('to')} ${maxValue}${currencySymbol}';
//  }
}
