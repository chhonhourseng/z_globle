import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CoScroll extends StatelessWidget {
  Widget child;
  ValueChanged<String> onRefresh;
  ScrollController scrollController;
  CoScroll({Key key, this.child, this.onRefresh, this.scrollController}) : super(key : key);
  @override
  Widget build(BuildContext context) {

    if(onRefresh != null) {
      return RefreshIndicator(
        onRefresh: () async {
          if (onRefresh != null) {
            return onRefresh('Change');
          }
        },
        child: CupertinoScrollbar(
          child: CustomScrollView(
            controller: scrollController,
            physics: BouncingScrollPhysics(),
            slivers: <Widget>[
              SliverToBoxAdapter(
                child: child,
              ),
            ],
          ),
        ),
      );
    }
    return CupertinoScrollbar(
      child: CustomScrollView(
        controller: scrollController,
        physics: BouncingScrollPhysics(),
        slivers: <Widget>[
          SliverToBoxAdapter(
            child: child,
          ),
        ],
      ),
    );
  }
}
