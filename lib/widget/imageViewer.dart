import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import '../g_widget.dart' as gWidget;

class ImageViewer extends StatefulWidget {
  String url;
  ImageViewer({Key key, this.url}) : super(key: key);
  @override
  _ImageViewerState createState() => _ImageViewerState();
}

class _ImageViewerState extends State<ImageViewer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black.withOpacity(0.1),
      ),
      body: Container(
        child:  PhotoView.customChild(
          child: gWidget.imageUrlWidget(
              widget.url,
              fit: BoxFit.contain
          ),
        ),
      ),
    );
  }
}
