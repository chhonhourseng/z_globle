import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

import '../g.dart' as gCo;
import '../g_widget.dart' as gWidget;
import '../g_form.dart' as gForm;
import '../g_model.dart';

class CoImageGallery extends StatefulWidget {

  List<CoImageGalleryModel> items;
  PageController pageController;
  int initialIndex;
  CoImageGallery({this.items, this.initialIndex}) : pageController = PageController(initialPage: initialIndex);

  @override
  _CoImageGalleryState createState() => _CoImageGalleryState();
}

class _CoImageGalleryState extends State<CoImageGallery> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
          color: Colors.transparent,
        child: Stack(
          children: [
            widget.items != null ? PhotoViewGallery.builder(
                itemCount: widget.items.length,
                scrollPhysics: BouncingScrollPhysics(),
                pageController: widget.pageController,
                backgroundDecoration: BoxDecoration(
                    color: Colors.transparent
                ),
                builder: (context, index){
                  return PhotoViewGalleryPageOptions.customChild(
                      heroAttributes: PhotoViewHeroAttributes(tag: 'coHeroPhotoViewGallery${index}'),
                      child: Container(
                          padding: EdgeInsets.only(top: 70),
                          child: gWidget.imageUrlWidget(
                              widget.items[index].url,
                              fit: BoxFit.contain
                          )
                      )
                  );
                }
            ) : SizedBox(height: 0, width: 0),
            Positioned(
                left: 10,
                top: 25,
                child: IconButton(
                  icon: Icon(Icons.arrow_back, color: Colors.white,),
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                )
            )
          ],
        )
      ),
    );
  }
}
