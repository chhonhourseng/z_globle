import 'package:flare_flutter/flare_actor.dart';
import 'dart:async';
import 'package:async/async.dart';
import 'g_widget.dart';
import 'package:path/path.dart' as path;
import 'package:flutter/cupertino.dart';
import 'package:html/parser.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:intl/intl.dart';
import 'package:pretty_json/pretty_json.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:path_provider/path_provider.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'g_model.dart';
import 'g_widget.dart' as gWidget;
import 'helper/stringExtension.dart';
//import 'package:scoped_model/scoped_model.dart';
import 'helpers/renderRowData.dart';

Color mainColor = Color(0XFF034da2);
enum coPostType {GET, POST}
enum coKeyboardType {INT, DOUBLE, FLOAT}


final coNavigatorAppKey = GlobalKey<NavigatorState>();
final BuildContext coContext = coNavigatorAppKey.currentState.overlay.context;

String defaultImageAsset = 'assets/img/placeholder_image.jpg';

String assetAPIVersion = "0.2";
String userSessionVersion = "2.0";
bool debug = false;
bool apiWithTimestamp = false;
Map coAppLanguage;
String lang = 'en';
List<LanguageModel> languageModel;
LanguageModel languageModelVal;
String formatDateSql = 'yyyy-MM-dd';
String formatDateDisplay = 'd MMM, y';
String baseUrl = '';
String baseUrlImg = '';
String currencyDefaultSymbols = '₭';



void initialize({bool coDebug, bool coApiWithTimestamp, Color coMainColor, String coBaseUrl, String coBaseUrlImg, String coUserSessionVersion, String coDefaultImageAsset, String coCurrencyDefaultSymbols, String coDefaultLang, List<LanguageModel> coLanguageModel}){
  if(coDebug != null){
    debug = coDebug;
  }
  if(coApiWithTimestamp != null){
    apiWithTimestamp = coApiWithTimestamp;
  }
  if(coMainColor != null){
    mainColor = coMainColor;
  }
  if(coBaseUrl != null && coBaseUrl != ''){
    baseUrl = coBaseUrl;
  }
  if(coBaseUrlImg != null && coBaseUrlImg != ''){
    baseUrlImg = coBaseUrlImg;
  }
  if(coUserSessionVersion != null && coUserSessionVersion != ''){
    userSessionVersion = coUserSessionVersion;
  }
  if(coUserSessionVersion != null && coUserSessionVersion != ''){
    userSessionVersion = coUserSessionVersion;
  }
  if(coDefaultImageAsset != null && coDefaultImageAsset != ''){
    defaultImageAsset = coDefaultImageAsset;
  }
  if(coCurrencyDefaultSymbols != null && coCurrencyDefaultSymbols != ''){
    currencyDefaultSymbols = coCurrencyDefaultSymbols;
  }
  if(coDefaultLang != null && coDefaultLang != ''){
    lang = coDefaultLang;
  }
  if(coLanguageModel != null){
    languageModel = coLanguageModel;
  }
}
String getCurrentTimestamp(){
  return DateTime.now().millisecondsSinceEpoch.toString();
}
int dateToInt(DateTime myTime){
  return int.parse('${formatDateOfDay(myTime, format: 'yyyyMMdd')}');
}

Future<File> getImageFileFromAsset(@required Asset imageAsset)async{
  final filePath = await FlutterAbsolutePath.getAbsolutePath(imageAsset.identifier);
  return File(filePath);
}
Future<File> getImageFileFromAssetString(@required String path) async {
  final byteData = await rootBundle.load('assets/$path');
  final file = File('${(await getTemporaryDirectory()).path}/$path');
  await file.writeAsBytes(byteData.buffer.asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
  return file;
}
Future<File> reSizeImageWith({@required File file,int maxSize : 600})async{
  ImageProperties properties = await FlutterNativeImage.getImageProperties(file.path);
  if(properties.width > maxSize || properties.height > maxSize){
    File compressedFile;
    if(properties.width > properties.height){
      compressedFile = await FlutterNativeImage.compressImage(file.path, quality: 80,
          targetWidth: maxSize,
          targetHeight: (properties.height * maxSize / properties.width).round());
    } else {
      compressedFile = await FlutterNativeImage.compressImage(file.path, quality: 80,
          targetWidth: (properties.width * maxSize / properties.height).round(),
          targetHeight: maxSize
      );
    }
    return compressedFile;
  } else {
    return file;
  }
}
void changeLanguage(context, {String title , ValueChanged<String> onChanged}){
  gWidget.coDialog(context,
      title: title == null ? 'choose_languages'.translate().toFirstUppercaseWord() : title,
      build: (languageModel == null ? false : languageModel.length > 0) ? new Column(
        mainAxisSize: MainAxisSize.min,
        children: List.generate(languageModel.length,(index){
          return RadioListTile(
            title: Row(
              children: <Widget>[
                Container(
                    width: 40,
                    child: Image.asset('${languageModel[index].assetImage}')
                ),
                SizedBox(width: 5,),
                Text('${languageModel[index].name}',),

              ],

            ),
            value: languageModel[index].key,
            groupValue: lang,
            onChanged: (value) {
              languageModelVal = languageModel[index];
              lang = languageModel[index].key;
              Navigator.of(context).pop();
              if(onChanged != null){
                onChanged(value);
              }
            },
          );
        }),
      ) :
      Text('no_languages'.translate().toFirstUppercaseWord()),
  ) ;
}
/*void printDebugScope({@required String output, String note : '', Map<String, dynamic> fields : null}){
  final BuildContext context = coContext;
  CoAppModel model = ScopedModel.of(context);
  model.printDebug(output, note: note, fields: fields);
}
CoAppModel getAppModel(){
  final BuildContext context = coContext;
  CoAppModel model = ScopedModel.of(context);
  return model;
}*/
void coCloseAndBackNavigatorWithDebug(BuildContext context){
  if(debug == false){
    Navigator.pop(context);
  }
}
String decodeString(String str) {
  return str
      .replaceAll('&amp;', '/')
      .replaceAll("&quot;", "\"")
      .replaceAll("&ldquo;", "“")
      .replaceAll("&rdquo;", "”")
      .replaceAll("<br>", "\n")
      .replaceAll("&gt;", ">")
      .replaceAll("&lt;", "<");
}
Widget loadingWidget({bool start = true}) {
  return IgnorePointer(
    child: Container(
      alignment: Alignment.center,
      color: Colors.transparent,
      child: FlareActor(
        'assets/img/loading.flr',
        animation: 'start', //该动画定义的名字
        color: Theme.of(coContext).primaryColor,
        isPaused: start != true,
      ),
    ),
  );
}
class CoKeyboardNumberConfigModel {
  TextEditingController textController;
  FocusNode focusNode;
  ValueChanged<String> onChanged;
  bool counter;
  coKeyboardType keyboardType;
  CoKeyboardNumberConfigModel({@required this.textController, @required this.focusNode, this.onChanged, this.counter : false, this.keyboardType : coKeyboardType.INT});
}

void showKeyboardNumber({lang : 'en'}) {
  showModalBottomSheet(
      /*shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(5.0))),*/
      backgroundColor: Colors.grey[350],
      barrierColor : Colors.transparent,
      context: coContext,
      isScrollControlled: true,
      builder: (context) => keyboardNumberWidget(context, lang: lang));
}
double keyboardHeight = 300;
bool keyboardShow = false;

class KeyboardNumberBuilder extends StatefulWidget {
  BuildContext context;
  List<CoKeyboardNumberConfigModel> config;
  Widget child;
  KeyboardNumberBuilder({
    Key key,
    @required this.context,
    @required this.config,
    @required this.child,
  }) : super(key: key);
  @override
  _keyboardNumberBuilderState createState() => _keyboardNumberBuilderState();
}

class _keyboardNumberBuilderState extends State<KeyboardNumberBuilder> {
  //CoAppModel coModel = getAppModel();
  List<double> bottom = [];
  List<double> opacity = [];
  _focusNodeReRender(FocusNode _focusNode) {
    print('focusNode updated: hasFocus: ${_focusNode.hasFocus}');
    setState(() {
      // Re-renders
    });
    new Future.delayed(const Duration(milliseconds: 100), (){
      setState(() {
        int i = 0;
        for(final index in widget.config){
          if(index.focusNode.hasFocus){
            //var hasFocusIndex = widget.config.indexWhere((row) => row.focusNode.hasFocus);
            bottom[i] = 20;
            opacity[i] = 1;
          } else {
            bottom[i] = 0;
            opacity[i] = 0;
          }
          i+=1;
        }
      });
    });
    /*var hasFocusIndex = widget.config.indexWhere((row) => row.focusNode.hasFocus);
    print(hasFocusIndex);
    if(hasFocusIndex > -1){
      new Future.delayed(const Duration(milliseconds: 100), (){
        setState(() {
          bottom[hasFocusIndex] = 0.0;
        });
      });

    }*/

  }

  @override
  void initState() {
    // TODO: implement initState
    //coModel.keyboardAction(false);
    setBottomVal();
    for(final index in widget.config){
      index.focusNode.addListener((){
        _focusNodeReRender(index.focusNode);
      });
    }
    super.initState();

  }
  void setBottomVal(){
    for(final index in widget.config){
      bottom.add(0);
      opacity.add(0);
    }
  }
  @override
  Widget build(BuildContext context) {
          double keyboardHeight = MediaQuery.of(context).viewInsets.bottom;
          double coWidth = MediaQuery.of(context).size.width;
          Future<String> calculator(String value, {bool add : true})async{
            //if(value != null && value.length > 0){
              //value = value.substring(0, value.length - 1);
              int number =  int.tryParse(value != null && value != '' && value != 'null' ? value : '0');
              if(number != null && number != 'null' ){
                return '${add ? number + 1 : (number > 0 ? number - 1 : 0) }';
              }
              return value;
            //}
          }
          return Container(
            child: Column(
              children: [
                Expanded(child: widget.child),
                keyboardHeight == 0.0 ? Column(
                  children: List.generate(widget.config.length, (index){
                    return keyboardHeight < 1 && widget.config[index].focusNode.hasFocus ?
                      AnimatedOpacity(
                        duration: Duration(milliseconds: 500),
                        opacity: opacity[index],
                        child: Column(
                          children: [
                            widget.config[index].counter ? Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(width: coWidth - coWidth * 0.85),
                                  IconButton(icon: Icon(Icons.remove), onPressed: (){
                                    calculator(widget.config[index].textController.text, add: false).then((number){
                                      widget.config[index].onChanged('${number}');
                                      setState(() {
                                        /// code
                                      });
                                    });

                                    /// code
                                  }),
                                  Container(width: coWidth - coWidth * 0.95),
                                  Expanded(
                                    child: Center(
                                      child: Text(
                                        '${widget.config[index].textController.text == '' ? '0' : '${widget.config[index].textController.text}'}',
                                        style: TextStyle(fontSize: 20),
                                        overflow: TextOverflow.clip,
                                      ),
                                    ),
                                  ),
                                  Container(width: coWidth - coWidth * 0.95),
                                  IconButton(icon: Icon(Icons.add), onPressed: (){
                                    /// code
                                    calculator(widget.config[index].textController.text).then((number){
                                      widget.config[index].onChanged('${number}');
                                      setState(() {
                                        /// code
                                      });
                                    });
                                  }),
                                  Container(width: coWidth - coWidth * 0.85),
                                ],
                              ),
                            ) : coZero(),
                            Container(
                              //height: keyboardHeight,
                                child: keyboardNumberWidget(context,
                                    keyboardType: widget.config[index].keyboardType,
                                    onChanged: (val){
                                      if(val == 'done'){
                                        widget.config[index].focusNode.unfocus();
                                      } else {
                                        if(val == 'delete'){
                                          String value = widget.config[index].textController.text;
                                          if(value != null && value.length > 0){
                                            value = value.substring(0, value.length - 1);
                                            widget.config[index].onChanged(value);
                                          }
                                        } else {
                                          String value = widget.config[index].textController.text;
                                          widget.config[index].onChanged('${value}${val}');
                                        }
                                      }
                                      setState(() {
                                        /// code
                                      });
                                    })
                            ),
                            AnimatedContainer(
                                duration: Duration(milliseconds: 200),
                                height: bottom[index],
                                color: Colors.grey[350],
                            )
                          ],
                        ),
                      )
                    : coZero();
                  })
                ): coZero()
              ],
            ),
          );

  }
}

Widget keyboardNumberWidget(context, {lang : 'en', ValueChanged<String> onChanged, keyboardType : coKeyboardType.INT} ){
  return Container(
    color: Colors.grey[350],
    //padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          //color: Colors.grey[350],
          padding: EdgeInsets.only(left: 10, right: 10, top: 10),
          child: CustomScrollView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            slivers: <Widget>[
              SliverGrid(
                gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(
                  ///no.of items in the horizontal axis
                    crossAxisCount: 4,
                    childAspectRatio: 1.5
                ),

                ///Lazy building of list
                delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                    Widget action = coZero();
                    Widget coWidget = Container(
                        width: MediaQuery.of(context).size.width / 4,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: coNumber[index]['key'] == 'blank' || coNumber[index]['key'] == ',' ? Colors.grey[400] : (coNumber[index]['key'] == '.' && keyboardType == coKeyboardType.INT ? Colors.grey[400] : Colors.white)
                        ),
                        margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                        child: coNumber[index]['key'] == 'blank' || coNumber[index]['key'] == ',' ? coZero() : Center(
                          child: Text(coNumber[index]['${lang}'], style: TextStyle(fontSize: 20, color: coNumber[index]['key'] != 'done' ? Colors.black87 : Colors.blueAccent,fontWeight: FontWeight.bold),),
                        )
                    );
                    if(coNumber[index]['key'] == 'delete') {
                      coWidget = Container(
                          width: MediaQuery.of(context).size.width / 4,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white
                          ),
                          margin: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                          child: Center(
                            child: Icon(Icons.backspace_outlined),
                          )
                      );
                    }
                    return Material(
                        color: Colors.grey[350],
                        child: InkWell(
                        onTap: (){
                          if(onChanged != null && coNumber[index]['key'] != 'blank'){
                            if(coNumber[index]['key'] == '.' && keyboardType == coKeyboardType.INT){
                              /// code
                            } else {
                              onChanged(coNumber[index]['key'].toString());
                            }

                          }
                          // CoAppModel appModel = getAppModel();
                          // appModel.keyboardValAction(coNumber[index]['key'].toString());
                          ///code
                        },
                        child : coWidget
                        )
                    );
                  },

                  /// Set childCount to limit no.of items
                  childCount: coNumber.length,
                ),
              ),
            ],
          ),
        ),
        //SizedBox(height: 10),
      ],
    ),
  );
}
Future<bool> scrollRenderDataController(@required ScrollController _controller,{@required RenderRowData, double offset : 10}) async {
  if(RenderRowData.dataRow.noMore == false){
    if ((_controller.offset + offset) >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      return true;
    } else {
      return Future.error(false);
    }
    return Future.error(false);
    /*if (_controller.offset <= _controller.position.minScrollExtent &&
          !_controller.position.outOfRange) {
      }*/
  }
}
void scrollToTop(@required ScrollController _controller){
  _controller.animateTo(
    0.0,
    curve: Curves.easeOut,
    duration: const Duration(milliseconds: 300),
  );
}

Future<dynamic> get({String url : '', Map<String, dynamic> fields : null,  bool addBaseUrl : false}) async {
  Map errorsList = {
    'error' : 1
  };
  try {
    if(url == ''){
      errorsList.addAll({'message' :  'no url'});
      //throw Exception(errorsList.toString());
      return Future.error(json.encode(errorsList));
    }
    if(apiWithTimestamp){
        fields['v'] = getCurrentTimestamp();
    }
    if(fields != null){
      if(fields.length > 0){
        int coI = 0;
        fields.forEach((name, val) async {
          coI+=1;
          String coSymbolc = coI == 1 ? (url.contains('?') == true ? '&' : '?') : '&';
          url += '${coSymbolc}${name}=${val}';
        });
      }
    }

    if(addBaseUrl){
      url = '${baseUrl}${url}';
    }
    //print('url : ' + url);
    var coUri = Uri.parse(url);
    if(debug){
      print('============url==========${coUri}');
    }
    var response = await http.get(coUri);
    if (response.statusCode == 200) {
      try {
        return response.body;
        //return await utf8.decoder.bind(response.stream).join();
      } catch(e) {
        debugPrint("Error $e");
        errorsList.addAll({'message' :  "Error ${e}"});
        //throw Exception(errorsList.toString());
        return Future.error(json.encode(errorsList));
      }
    } else {
      errorsList.addAll({'message' :  'server error'});
      errorsList.addAll({'responseStatusCode' :  response.statusCode.toString()});
      errorsList.addAll({'response' :  Error().toString()});
      //throw Exception(errorsList.toString());
      return Future.error(json.encode(errorsList), StackTrace.fromString("This is its trace"));
    }
  } catch (e) {
    debugPrint("Error $e");
    errorsList.addAll({'message' :  "Error ${e}"});
    //throw Exception(errorsList.toString());
    return Future.error(json.encode(errorsList));
  }
}
Future<dynamic> post({String url : '', Map<String, dynamic> fields : null,  Map<String, File> files : null, bool addBaseUrl : false}) async {
  Map errorsList = {
    'error' : 1
  };
  try {
    if(url == ''){
      errorsList.addAll({'message' :  'no url'});
      //throw Exception(errorsList.toString());
      return Future.error(json.encode(errorsList));
    }
    /*if(fields == null && files == null){
      errorsList.addAll({'message' :  'no fields and files'});
      throw errorsList.toString();
    }*/
    if(apiWithTimestamp){
      url = '${url}${url.contains('?') == true ? '&' : '?'}v=${getCurrentTimestamp()}';
    }
    if(addBaseUrl){
      url = '${baseUrl}${url}';
    }
    if(debug){
      print('============url==========${url}');
    }
    var coUri = Uri.parse(url);

    var request = http.MultipartRequest("POST", coUri);
    if(fields != null){
      if(fields.length > 0){
        fields.forEach((name, val) async {
          request.fields['${name}'] = '${val}';
        });
      }
    }
    if(files != null){
      if(files.length > 0){
        files.forEach((name, val) async {
          if(val != null && name != '' && name != null){
            var stream = http.ByteStream(DelegatingStream.typed(val.openRead()));
            var length = await val.length();
            request.files.add(http.MultipartFile("${name}", stream, length, filename: path.basename(val.path)));
          }
        });
      }
    }

    var response = await request.send();
    if (response.statusCode == 200) {
      try {
        return response.stream.bytesToString();
        //return await utf8.decoder.bind(response.stream).join();
      } catch(e) {
        debugPrint("Error $e");
        errorsList.addAll({'message' :  "Error ${e}"});
        //throw Exception(errorsList.toString());
        return Future.error(json.encode(errorsList));
      }
    } else {
      errorsList.addAll({'message' :  'server error'});
      errorsList.addAll({'responseStatusCode' :  response.statusCode.toString()});
      errorsList.addAll({'response' :  Error().toString()});
      //throw Exception(errorsList.toString());
      return Future.error(json.encode(errorsList), StackTrace.fromString("This is its trace"));
    }
  } catch (e) {
    debugPrint("Error $e");
    errorsList.addAll({'message' :  "Error ${e}"});
    //throw Exception(errorsList.toString());
    return Future.error(json.encode(errorsList));
  }


}
List statusList = [
  {
    'id': 'Activated',
    'name': 'Activated',
  },
  {
    'id': 'Approved',
    'name': 'Approved',
  },
  {
    'id': 'Pending',
    'name': 'Pending',
  },
  {
    'id': 'Closed',
    'name': 'Closed',
  },
  {
    'id': 'Declined',
    'name': 'Declined',
  },
];
final formatter = new NumberFormat("###,###,###,###.##");

String numberFormatterToString({String stringNumber, bool currency : false, String currencySymbol}){
  double numberFormat = double.tryParse(stringNumber != null && stringNumber != '' && stringNumber != 'null' ? stringNumber : '0');
  double n = num.parse(numberFormat.toStringAsFixed(2));
  List numberArr = '${n}'.split(".");
  return '${formatter.format(int.tryParse(checkKeyList(numberArr, 0) ? numberArr[0] : '0'))}' + '${checkKeyList(numberArr, 1) ? (numberArr[1] == '0' || numberArr[1] == '' ? '' : '.${numberArr[1]}') : ''}${currencySymbol == null  ? (currency ? ' ' + currencyDefaultSymbols : '') : currencySymbol}';
}
bool checkKeyList(List arr, int length){
  if(arr == null && length > 0){
    return false;
  }
  return arr.length >= length ? true : false;
}
bool checkKeyMap(Map arr, dynamic key){
  if(arr == null && key != null){
    return false;
  }
  return arr.containsKey(key);
}
void launchMapsUrl(double lat, double lon) async {
  final url = 'https://www.google.com/maps/search/?api=1&query=$lat,$lon';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
String getPrettyJSONString(String jsonText, {String title : 'OUTPUT'}){
  try {
    var jsonTexts = json.encode(json.decode(jsonText));
    var jsonMap = json.decode(jsonTexts);
    var jsons = <String, dynamic>{
      title : jsonMap,
    };
    return prettyJson(jsons, indent: 5);
  } catch (e) {
    return jsonText;
  }
  return jsonText;

  /*var encoder = new JsonEncoder.withIndent("     ");
  return encoder.convert(jsonObject);*/
}
void coAction(context,{String title, List<Widget> actions} ){
  /*CupertinoActionSheetAction(
    child: Text("Disable"),
    isDestructiveAction: true,
    onPressed: () {
      Navigator.pop(context);

      });*/
  final action = CupertinoActionSheet(
    title: Text(
      title == null ? 'action'.translate().toFirstUppercase() : title,
      style: TextStyle(fontSize: 20),
    ),
    message: Column(
      children: <Widget>[
        Text(
          "click_any_action".translate().toFirstUppercase(),
          style: TextStyle(fontSize: 15.0),
        ),
      ],
    ),
    actions: actions,
    cancelButton: CupertinoActionSheetAction(
      child: Text("close".translate().toFirstUppercase()),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
    ),
  );
  showCupertinoModalPopup(
      context: context, builder: (context) => action);
}


Widget listRow(context, {String left: 'Name', String right: 'Mai Youry', bool boldRight: false, bool boldLeft: false, Widget rightWidget, int maxLines: 3}){
  TextStyle textStyle = TextStyle();
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 20),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width / 2 - MediaQuery.of(context).size.width / 4,
          child: Text(
            left, style: boldLeft ? textStyle.copyWith(fontWeight: FontWeight.bold, fontSize: 16) : textStyle,
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
          ),
        ),
        SizedBox(width: 15,),
        rightWidget == null ? Container(
          width: MediaQuery.of(context).size.width / 2,
          child:  Text(
            right, style: boldRight ? textStyle.copyWith(fontWeight: FontWeight.bold, fontSize: 16) : textStyle,
            overflow: TextOverflow.ellipsis,
            maxLines: maxLines,
          ),
        ) : rightWidget,
      ],
    ),
  );
}
Widget paymentStatus(String text){
  Color color = Colors.green;
  if(text == 'Closed'){
    color = Colors.red;
  } else if(text == 'Pending'){
    color = Colors.orange;
  } else if(text == 'Declined'){
    color = Colors.black45;
  } else if(text == 'Activated'){
    color = Colors.blue;
  }
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 9, vertical: 2),
    decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(10)
    ),
    child: Text(
      '${text}',
      style: TextStyle(color: Colors.white),
    ),
  );
}
Widget loanPersonStatus(String text){
  Color color = Colors.green;
  if(text == 'Loan Officer'){
    color = Colors.orange;
  } else if(text == 'Pending'){
    color = Colors.orange;
  } else if(text == 'Declined'){
    color = Colors.black45;
  } else if(text == 'Activated'){
    color = Colors.blue;
  }
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 9, vertical: 4),
    decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(10)
    ),
    child: Container(
      child: Row(
        children: <Widget>[
          Icon(Icons.person, size: 17, color: Colors.white,),
          SizedBox(width: 6,),
          Text(
            text,
            style: TextStyle(
                fontSize: 13.0,
                color: Colors.white
            ),
          ),
        ],
      ),
    ),
  );
}
paymentDate(String date){
  List dateArr = date.split("-");
  final dt = DateTime(int.parse(dateArr[0]), int.parse(dateArr[1]), int.parse(dateArr[2]), 7, 00);
  return formatDateOfDay(dt, format: 'd MMM, y');
}
stringDateTimeToFormatDate(String dateTime){
  DateTime dt = new DateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime);
  return formatDateOfDay(dt, format: 'd MMM, y');
}
stringDateToFormatDate(String dateTime){
  DateTime dt = new DateFormat("yyyy-MM-dd").parse(dateTime);
  return formatDateOfDay(dt, format: 'd MMM, y');
}
stringDateToFormatDateTimeObject(String dateTime){
  DateTime dt = new DateFormat("yyyy-MM-dd").parse(dateTime);
  return dt;
}
String parseHtmlString(String htmlString) {
  var document = parse(htmlString);
  String parsedString = parse(document.body.text).documentElement.text;
  return parsedString;
}

Future<File> getlocalfile(String fileName) async {
  final directory = await path_provider.getApplicationDocumentsDirectory();
  return File('${directory.path}/${fileName}');
}

Future<File> writeTextToLocalFile(String fileName, String text) async {
  final file = await getlocalfile(fileName);
  return file.writeAsString(text);
}

Future readTextFromLocalFile(String fileName) async {
  String content;
  try {
    final file = await getlocalfile(fileName);
    content = await file.readAsString();
    //print('============${content}');
    return content;
  } catch (e) {
    // print(e);
    return false;
  }
  content = 'null';
  return content;
}

prepareDataJsonFileWithAPI(
    {String fileName, Future getData(bool firstTime), pastData(data)}) async {
  var jsonFileAPI = await readTextFromLocalFile('${fileName}-${assetAPIVersion}');
  if (jsonFileAPI != false) {
    var dataFile = json.decode(jsonFileAPI);
    pastData(dataFile);
    var data = await getData(false);
    if (data != false) {
      pastData(data);
      await writeTextToLocalFile(
          '${fileName}-${assetAPIVersion}', json.encode(data));
    }
  } else {
    var data = await getData(true);
    if (data != false) {
      pastData(data);
      await writeTextToLocalFile(
          '${fileName}-${assetAPIVersion}', json.encode(data));
    }
  }
}

formatTimeOfDay(TimeOfDay tod,
    {returnObject: false, return24FormatTime: false}) {
  final now = new DateTime.now();
  final dt = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
  if (returnObject) {
    return dt;
  } else if (return24FormatTime) {
    final format = DateFormat.Hm(); //"6:00 AM"
    return format.format(dt);
  }
  final format = DateFormat.jm(); //"6:00 AM"
  return format.format(dt);
}

formatDateOfDay(DateTime tod, {returnObject: false, format : 'd/M/y'}) {
  final now = new DateTime.now();
  final dt = DateTime(tod.year, tod.month, tod.day, tod.hour, tod.minute);
  if (returnObject) {
    return dt;
  }
  final formats = DateFormat("${format}");
  return formats.format(dt);
}


  List coNumber = [
    {
      'key' : '1',
      'en' : '1',
      'kh' : '',
    },
    {
      'key' : '2',
      'en' : '2',
      'kh' : '',
    },
    {
      'key' : '3',
      'en' : '3',
      'kh' : '',
    },
    {
      'key' : 'delete',
      'en' : 'delete',
      'kh' : '',
    },
    {
      'key' : '4',
      'en' : '4',
      'kh' : '',
    },
    {
      'key' : '5',
      'en' : '5',
      'kh' : '',
    },
    {
      'key' : '6',
      'en' : '6',
      'kh' : '',
    },
    {
      'key' : 'done',
      'en' : 'Done',
      'kh' : 'Done',
    },
    {
      'key' : '7',
      'en' : '7',
      'kh' : '',
    },
    {
      'key' : '8',
      'en' : '8',
      'kh' : '',
    },
    {
      'key' : '9',
      'en' : '9',
      'kh' : '',
    },
    {
      'key' : '.',
      'en' : '.',
      'kh' : '.',
    },
    {
      'key' : 'blank',
      'en' : 'blank',
      'kh' : 'blank',
    },
    {
      'key' : '0',
      'en' : '0',
      'kh' : '0',
    },
    {
      'key' : 'blank',
      'en' : 'blank',
      'kh' : 'blank',
    },
    {
      'key' : ',',
      'en' : ',',
      'kh' : ',',
    },
  ];
