import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../g_form.dart';
/* ------------------------------------------------------------------------- */

class coStep extends StatefulWidget {
  List<CoStepModel> build;
  int index;
  ValueChanged<int> onBack;
  ValueChanged<int> onNext;
  ValueChanged<int> onDone;
  bool validation;

  coStep(
      {this.build,
      this.index = 1,
      this.validation: false,
      this.onBack,
      this.onNext,
      this.onDone});

  @override
  _coStepState createState() => _coStepState();
}

class _coStepState extends State<coStep> {
  @override
  Widget build(BuildContext context) {
    return widget.build != null
        ? Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: List.generate(widget.build.length, (index) {
                      return Expanded(
                        flex: index == 0 ? 2 : 3,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Divider(
                                color: widget.index >= (index + 1)
                                    ? Theme.of(context).accentColor
                                    : Colors.black45,
                                thickness: widget.index >= (index + 1) ? 2 : 1,
                              ),
                            ),
                            Container(
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(100),
                                    child: Container(
                                      width: 40,
                                      height: 40,
                                      color: widget.index >= (index + 1)
                                          ? Theme.of(context).accentColor
                                          : Colors.black45,
                                      child: Center(
                                          child: Text(
                                        '${(index + 1)}',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.bold),
                                      )),
                                    ))),
                          ],
                        ),
                      );
                    })),
              ),
              Expanded(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: List.generate(widget.build.length, (index) {
                      return Visibility(
                          visible: widget.index == (index + 1) ? true : false,
                          child: Expanded(
                              child: Container(
                            margin: EdgeInsets.only(top: 10),
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Colors.black.withOpacity(0.07),
                                  width: 1),
                              borderRadius: BorderRadius.all(Radius.circular(
                                      5.0) //         <--- border radius here
                                  ),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                widget.build[index].header != null ? widget.build[index].header : SizedBox(width: 0, height: 0,),
                                widget.build[index].title != null ?
                                Text(widget.build[index].title, style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 20,
                                    fontWeight:
                                    FontWeight.w600))
                                    : SizedBox(width: 0, height: 0,),
                                widget.build[index].headLine != null ? Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.only(
                                      top: 10, bottom: 10, left: 15),
                                  decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(0.05),
                                    border: Border(
                                      bottom: BorderSide(
                                          color:
                                              Colors.black.withOpacity(0.07)),
                                    ),
                                  ),
                                  margin: EdgeInsets.only(bottom: 5),
                                  child: widget.build[index].headLine,
                                ) : SizedBox(width: 0, height: 0,),
                                Expanded(
                                    child: Container(
                                  padding: EdgeInsets.only(right: 0, left: 0),
                                  child: Stack(
                                    alignment: AlignmentDirectional.bottomEnd,
                                    children: <Widget>[
                                      SingleChildScrollView(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              height: 10,
                                            ),
                                            widget.build[index].body,
                                            Container(
                                              height: 100,
                                            ),
                                          ],
                                        ),
                                      ),
                                      Positioned(
                                        child: Container(
                                          height: 70,
                                          child: Row(
                                            children: <Widget>[
                                              Expanded(
                                                  child: Visibility(
                                                visible:
                                                    index >= 1 ? true : false,
                                                child: coButton(
                                                  minWidth: 0.0,
                                                  color: Color.fromARGB(
                                                      255, 173, 171, 171),
                                                  text: 'Back',
                                                  onPressed: () {
                                                    setState(() {
                                                      if (widget.validation ==
                                                          false) {
                                                        widget.index = index;
                                                      } else {
                                                        if (widget.onBack !=
                                                            null) {
                                                          widget.onBack(index);
                                                        }
                                                      }
                                                    });
                                                  },
                                                ),
                                              )),
                                              SizedBox(
                                                width: 20,
                                              ),
                                              Expanded(
                                                  child: widget.index !=
                                                          widget.build.length
                                                      ? coButton(
                                                          minWidth: 0.0,
                                                          text: 'Next',
                                                          onPressed: () {
                                                            setState(() {
                                                              if (widget
                                                                      .validation ==
                                                                  false) {
                                                                widget.index =
                                                                    index + 2;
                                                              } else {
                                                                if (widget
                                                                        .onNext !=
                                                                    null) {
                                                                  widget.onNext(
                                                                      index +
                                                                          2);
                                                                }
                                                              }
                                                            });
                                                          },
                                                        )
                                                      : coButton(
                                                          minWidth: 0.0,
                                                          text: 'Done',
                                                          onPressed: () {
                                                            if (widget.onDone !=
                                                                null) {
                                                              widget.onDone(
                                                                  index + 1);
                                                            }
                                                          },
                                                        )),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )),
                              ],
                            ),
                          )));
                    }),
                  ),
                ),
              )
            ],
          )
        : Container(
            color: Colors.transparent,
          );
  }
}

class CoStepModel {
  Widget headLine;
  Widget header;
  String title;
  Widget body;

  CoStepModel({this.body, this.headLine, this.title, this.header});
}
