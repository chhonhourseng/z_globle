import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:z_globle/g_form.dart' as gForm;
import 'package:z_globle/g_widget.dart' as gWidget;
import 'package:z_globle/g.dart' as gCo;

typedef CoTimeWidgetBuilder = Widget Function(
    BuildContext context, String val);

class CoTime extends StatefulWidget {
  String text;
  String helperText;
  FormFieldValidator<String> validator;
  ValueChanged<TimeOfDay> onChanged;
  TimeOfDay initialTime;
  TextEditingController controller = TextEditingController();
  bool disable;
  CoTimeWidgetBuilder customBuilder;

  CoTime(
      {Key key,
        this.text,
        this.helperText,
        this.onChanged,
        this.controller,
        this.validator,
        this.disable: false,
        this.initialTime
      })
      : super(key: key);
  @override
  _CoDateState createState() => _CoDateState();
}

class _CoDateState extends State<CoTime> {
  DateTime dateNow = new DateTime.now();
  TimeOfDay times = new TimeOfDay.now();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Widget showDatePicker() {
    return CupertinoDatePicker(
      mode: CupertinoDatePickerMode.time,
      initialDateTime: new DateTime(dateNow.year, dateNow.month, dateNow.day, times.hour, times.minute,),
      //initialDateTime: widget.initialTime == null ? dateNow : new DateTime(dateNow.year, dateNow.month, dateNow.day, widget.initialTime.hour, widget.initialTime.minute, ),
      onDateTimeChanged: (newDateTime) {
        times = TimeOfDay(hour: newDateTime.hour, minute: newDateTime.minute);
      },
    );
  }

  void popUp(){
    if(widget.disable == false){
      times = new TimeOfDay(hour: widget.initialTime.hour, minute: widget.initialTime.minute);
      gWidget.coModalBottomSheet(context, text: widget.text, scrollAble: false ,withOK: true, build: showDatePicker(),
          onOK: (){
            widget.initialTime = new TimeOfDay(hour: times.hour, minute: times.minute);
            setState(() {
            });
            Navigator.of(context, rootNavigator: true).pop();
            if(widget.onChanged != null){
              widget.onChanged(times);
            }
          }
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if(widget.initialTime == null){
      widget.initialTime = new TimeOfDay(hour: times.hour, minute: times.minute);
    } else {
      times = new TimeOfDay(hour: widget.initialTime.hour, minute: widget.initialTime.minute);
    }
    return widget.customBuilder == null ? gForm.coTextField(
      disable: widget.disable,
      text: widget.text,
      readOnly: true,
      validator: widget.validator,
      controller: TextEditingController(text: '${gCo.formatTimeOfDay(widget.initialTime == null ? times : widget.initialTime)}'),
      iconRight: Icons.timer,
      onTap: (){
        popUp();
      }
    ) :
    InkWell(
      onTap: (){
        popUp();
      },
      child: widget.customBuilder(context, '${gCo.formatTimeOfDay(widget.initialTime == null ? times : widget.initialTime)}')
    );
  }
}
