import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:math' as math;
import '../g_form.dart';
/* ------------------------------------------------------------------------- */

class coTextField extends StatefulWidget {
  String text;
  String helperText;
  var textInputAction;
  bool readOnly;
  bool disable;
  bool numberOnly;
  IconData icon;
  IconData iconRight;
  coFormSize formSize;
  bool textArea;
  FormFieldValidator<String> validator;
  ValueChanged<String> onChanged;
  ValueChanged<String> onFieldSubmitted;
  GestureTapCallback onTap;
  Widget suffixIcon;
  FocusNode focusNode;
  Color fillColor;
  bool disableBorderColor;
  bool secureText;
  TextEditingController controller = TextEditingController();
  coTextField(
      {Key key,
        this.text,
        this.helperText,
        this.onChanged,
        this.onFieldSubmitted,
        this.onTap,
        this.controller,
        this.icon,
        this.iconRight,
        this.validator,
        this.suffixIcon,
        this.textInputAction : TextInputAction.done,
        this.formSize : coFormSize.normal,
        this.textArea: false,
        this.disable: false,
        this.readOnly : false,
        this.numberOnly: false,
        this.focusNode,
        this.fillColor,
        this.secureText : false,
        this.disableBorderColor : false
      })
      : super(key: key);

  @override
  _coTextFildState createState() => _coTextFildState();
}

class _coTextFildState extends State<coTextField> {
  var values;
  FocusNode _focusNode = new FocusNode();
  var field = new FieldProperty();

  @override
  void initState() {
    _focusNode = widget.focusNode == null ? _focusNode : widget.focusNode;
    WidgetsBinding.instance.addPostFrameCallback((_) => _onLoading(context));
    super.initState();
    _focusNode.addListener(_focusNodeReRender);
  }

  _focusNodeReRender() {
    setState(() {
      // Re-renders
    });
  }

  void _onLoading(BuildContext context) async {
    //print(widget.controller);
  }

  @override
  Widget build(BuildContext context) {
    if (widget.controller != null) {
      values = widget.controller.text;
    }
    return Theme(
      data: ThemeData(
          inputDecorationTheme: InputDecorationTheme(
            border: field.fieldBorder,
            contentPadding: field.fieldContentPadding,
          )),
      child: Container(
        child: TextFormField(
          obscureText : widget.secureText,
          textInputAction :  widget.textInputAction,
          focusNode: widget.focusNode == null ? _focusNode : widget.focusNode,
          maxLines: widget.textArea == true ? 10 : 1,
          minLines: widget.textArea == true ? 3 : 1,
          validator: widget.validator,
          readOnly: widget.disable ? widget.disable : widget.readOnly,
          controller: widget.controller,
          style: field.fieldStyle,
          onChanged: (value) {
            values = value;
            if (widget.onChanged != null) {
              widget.onChanged(value);
            }
          },
          onFieldSubmitted: (val){
            if (widget.onFieldSubmitted != null) {
              widget.onFieldSubmitted(val.toString());
            }
          },
          onTap: () {
            if (widget.onTap != null) {
              widget.onTap();
            }
          },
          keyboardType: widget.numberOnly == true
              ? TextInputType.numberWithOptions(decimal: true)
              : null,
          inputFormatters: widget.numberOnly == true
              ? [
            DecimalTextInputFormatter(decimalRange: 2),
            WhitelistingTextInputFormatter(RegExp(r'\d+\.?\d*'))
          ]
              : null,
          decoration: InputDecoration(
            fillColor:  widget.fillColor == null ? Colors.grey[150] : widget.fillColor,
            filled: widget.fillColor == null ? widget.disable : true,
            prefixIcon: widget.icon != null ? Icon(widget.icon) : null,
            suffixIcon: widget.suffixIcon == null ? widget.iconRight != null ? Icon(
              widget.iconRight,
              color: Colors.black87,
            ) : null : widget.suffixIcon,
            contentPadding: widget.formSize == coFormSize.normal ? field.fieldContentPadding : field.fieldContentPaddingSmall,
            helperText: widget.helperText,
            border: widget.disableBorderColor == false ? field.fieldBorder : field.fieldBorder.copyWith(borderSide:
            const BorderSide(color: Colors.transparent)),
            enabledBorder: widget.disableBorderColor == false ? field.fieldEnabledBorder : field.fieldEnabledBorder.copyWith(borderSide:
            const BorderSide(color: Colors.transparent)),
            focusedBorder: widget.disableBorderColor == false ? field.fieldBorder : field.fieldBorder.copyWith(borderSide:
            const BorderSide(color: Colors.transparent)),
            labelText: ("${widget.text}"),
            labelStyle: _focusNode.hasFocus
                ? field.fieldLabelHasFocusStyle
                : values != null && values != ''
                ? field.fieldLabelHasFocusStyle
                .copyWith(color: Colors.black54)
                : field.fieldLabelStyle,
            //labelStyle: TextStyle(color: Colors.red, fontSize: 20),
          ),
        ),
      ),
    );
  }
}

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({this.decimalRange})
      : assert(decimalRange == null || decimalRange > 0);

  final int decimalRange;

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, // unused.
      TextEditingValue newValue,
      ) {
    TextSelection newSelection = newValue.selection;
    String truncated = newValue.text;

    if (decimalRange != null) {
      String value = newValue.text;

      if (value.contains(".") &&
          value.substring(value.indexOf(".") + 1).length > decimalRange) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      } else if (value == ".") {
        truncated = "0.";

        newSelection = newValue.selection.copyWith(
          baseOffset: math.min(truncated.length, truncated.length + 1),
          extentOffset: math.min(truncated.length, truncated.length + 1),
        );
      }

      return TextEditingValue(
        text: truncated,
        selection: newSelection,
        composing: TextRange.empty,
      );
    }
    return newValue;
  }
}