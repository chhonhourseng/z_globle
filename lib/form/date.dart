import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../g_form.dart' as gForm;
import '../g_widget.dart' as gWidget;
import '../g.dart' as gCo;

typedef CoDateWidgetBuilder = Widget Function(
    BuildContext context, String val);

class CoDate extends StatefulWidget {
  String text;
  String helperText;
  FormFieldValidator<String> validator;
  ValueChanged<DateTime> onChanged;
  DateTime initialDateTime;
  TextEditingController controller = TextEditingController();
  bool disable;
  CoDateWidgetBuilder customBuilder;
  String dateFormatDisplay;

  CoDate(
      {Key key,
        this.text,
        this.helperText,
        this.onChanged,
        this.controller,
        this.validator,
        this.disable: false,
        this.initialDateTime,
        this.customBuilder,
        this.dateFormatDisplay : 'd MMM, y',
      })
      : super(key: key);
  @override
  _CoDateState createState() => _CoDateState();
}

class _CoDateState extends State<CoDate> {
  var now = new DateTime.now();
  DateTime dateTimes = new DateTime.now();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  Widget showDatePicker() {
    return CupertinoDatePicker(
      mode: CupertinoDatePickerMode.date,
      initialDateTime: dateTimes,
      onDateTimeChanged: (newDateTime) {
        dateTimes = newDateTime;
      },
    );
  }

  void popUp(){
    if(widget.disable == false) {
      dateTimes = new DateTime(widget.initialDateTime.year, widget.initialDateTime.month, widget.initialDateTime.day);
      gWidget.coModalBottomSheet(context, text: widget.text, scrollAble: false ,withOK: true, build: showDatePicker(),
        onOK: (){
          widget.initialDateTime = new DateTime(dateTimes.year, dateTimes.month, dateTimes.day);
          setState(() {
          });
          Navigator.of(context, rootNavigator: true).pop();
          if(widget.onChanged != null){
            widget.onChanged(dateTimes);
          }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if(widget.initialDateTime == null){
      widget.initialDateTime = new DateTime(dateTimes.year, dateTimes.month, dateTimes.day);
    } else {
      dateTimes = new DateTime(widget.initialDateTime.year, widget.initialDateTime.month, widget.initialDateTime.day);
    }
    return widget.customBuilder == null ? gForm.coTextField(
      disable: widget.disable,
      text: widget.text,
      readOnly: true,
      validator: widget.validator,
      controller: TextEditingController(text: '${gCo.formatDateOfDay(widget.initialDateTime == null ? dateTimes : widget.initialDateTime, format: widget.dateFormatDisplay)}'),
      iconRight: Icons.date_range,
      onTap: (){
        popUp();
      },
    ) :
    InkWell(
      onTap: (){
        popUp();
      },
      child: widget.customBuilder(context, '${gCo.formatDateOfDay(widget.initialDateTime == null ? dateTimes : widget.initialDateTime, format: widget.dateFormatDisplay)}')
    );
  }
}
