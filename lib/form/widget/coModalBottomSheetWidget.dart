import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../../helpers/dataRowForDropdown.dart';
import '../../g_widget.dart' as gWidget;
import '../../g.dart' as gCo;
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class coModalBottomSheetWidget extends StatefulWidget {
  List list;
  String id;
  int selectedIndex;
  String selectedId;
  bool returnIndex;
  DataRowForDropDown dataRow;
  ValueChanged<String> onChanged;
  ValueChanged<bool> onLoadMore;
  bool noSelectItemRow;


  coModalBottomSheetWidget(
      {Key key,
        this.list,
        this.id,
        this.onChanged,
        this.selectedIndex: 0,
        this.selectedId,
        this.returnIndex: false,
        this.dataRow,
        this.onLoadMore,
        this.noSelectItemRow : true,
      })
      : super(key: key);

  @override
  _coModalBottomSheetState createState() => _coModalBottomSheetState();
}

class _coModalBottomSheetState extends State<coModalBottomSheetWidget> {
  ItemScrollController _scrollController = ItemScrollController();
  ScrollController _controller;
  final ItemPositionsListener itemPositionsListener = ItemPositionsListener.create();
  var needToPaginate;

  _coScrollListener() {
      if(widget.list.length > 5){
        if(widget.dataRow.dropDownDataRow.noMore == false){
          if ((_controller.offset + 10) >= _controller.position.maxScrollExtent &&
              !_controller.position.outOfRange) {
            widget.onLoadMore(true);
          }
        }
      }
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => _onLoading(context));
    if(widget.dataRow == null ? true : (!widget.dataRow.wrapIs)){
      itemPositionsListener.itemPositions.addListener(() {
        /*needToPaginate = itemPositionsListener.itemPositions.value.firstWhere(
              (ItemPosition position) => position.index == currentMax, orElse: () => null)
          ?.itemTrailingEdge <= 1.0 ?? false;*/
        //print(itemPositionsListener.itemPositions.value.elementAt(0).index + );
        if(widget.list.length > 5){
          needToPaginate = itemPositionsListener.itemPositions.value.firstWhere(
                  (ItemPosition position) => position.index == widget.list.length - 10, orElse: () => null)
              ?.itemTrailingEdge;
          //needToPaginate = itemPositionsListener.itemPositions.value.firstWhere((ItemPosition position) => position.index == widget.list.length);
          //print(needToPaginate);
          if(needToPaginate != null){
            if(needToPaginate <= 1.0){
              widget.onLoadMore(true);
            }
          }
        }
      });
    } else {
      _controller = ScrollController();
      _controller.addListener(_coScrollListener);
    }

  }

  void _onLoading(BuildContext context) async {
    if (widget.selectedIndex != null && widget.selectedIndex > 5 && (widget.dataRow == null ? true : (!widget.dataRow.wrapIs))) {
      _scrollController.jumpTo(index: widget.selectedIndex - 1);
    }
  }

  Widget renderOneItem(index){
    if(widget.list.length > index){
      if(widget.dataRow == null){
        return gWidget.renderItems(
          list: widget.list,
          id: widget.id.toString(),
          index: index,
          onChanged: (value) {
            //print(value);
            if (widget.onChanged != null) {
              if (widget.returnIndex == false) {
                //print('on change ${widget.list[index]['id']}');
                //print('${value}');
                final idVal = widget.list[index]['id'].toString();
                //print("idVal : ${idVal}");
                widget.onChanged('${idVal}');
              } else {
                widget.onChanged(index.toString());
              }
            }
            setState(() {
              widget.selectedIndex = index;
            });
            Navigator.of(context).pop();
          },
        );
      }
      return widget.dataRow.renderItems(
        list: widget.list,
        id: widget.id.toString(),
        index: index,
        onChanged: (value) {
          //print(value);
          if (widget.onChanged != null) {
            if (widget.returnIndex == false) {
              //print('on change ${widget.list[index]['id']}');
              //print('${value}');
              final idVal = widget.list[index]['id'].toString();
              //print("idVal : ${idVal}");
              widget.onChanged('${idVal}');
            } else {
              widget.onChanged(index.toString());
            }
          }
          setState(() {
            widget.selectedIndex = index;
          });
          Navigator.of(context).pop();
        },
      );
    }
    if(widget.dataRow!= null){
      if(widget.dataRow == null ? true : (!widget.dataRow.wrapIs)){
        return progressWidget();
      }
    }
    return SizedBox(width: 0, height: 15,);
  }

  Widget progressWidget(){
    if(!widget.dataRow.dropDownDataRow.noMore){
      return Center(
        child: Container(
            width: 30,
            height: 30,
            margin: EdgeInsets.only(top: 15, bottom: 15),
            child: CircularProgressIndicator()
        ),
      );
    }
    return SizedBox(width: 0, height: 15,);
  }

  @override
  Widget build(BuildContext context) {
    //print('=========id=====${widget.id.toString()}');

    Widget renderItem = ScrollablePositionedList.builder(
        physics: BouncingScrollPhysics(),
        itemScrollController: _scrollController,
        itemPositionsListener: itemPositionsListener,
        itemCount: widget.list != null ? ((widget.dataRow == null ? true : (!widget.dataRow.wrapIs)) ? widget.list.length + 1 : 0) : 0,
        itemBuilder: (context, index) {
          return renderOneItem(index);
        });
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          child: CupertinoScrollbar(
            child: (widget.dataRow == null ? true : (!widget.dataRow.wrapIs)) ? renderItem :
                gWidget.CoScroll(
                  scrollController: _controller,
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: widget.dataRow.wrapPaddingHorizontal, vertical: widget.dataRow.wrapPaddingVertical),
                    child: Column(
                      children: [
                        GridView.count(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            crossAxisCount: widget.dataRow.wrapCrossAxisCount,
                            crossAxisSpacing: widget.dataRow.wrapCrossAxisSpacing,
                            mainAxisSpacing: widget.dataRow.wrapCrossAxisSpacing,
                            childAspectRatio: widget.dataRow.wrapAspectRatioChild,
                            children: List.generate(widget.list != null ? widget.list.length : 0, (index){
                              return renderOneItem(index);
                            }),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        progressWidget()
                      ],
                    ),
                  ),
                )
          ),
        ),
      ],
    );
  }
}