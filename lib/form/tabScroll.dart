import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'models/models.dart';
/* ------------------------------------------------------------------------- */

const appBarHeight = 200.0;
const expandedBarHeight = 150.0;
const tabHeight = 50.0;

class TabScroll extends StatefulWidget {
  List<FormTabScrollModel> build;
  TabScroll(
      {this.build,
      });
  @override
  _TabScrollState createState() => _TabScrollState();
}

class _TabScrollState extends State<TabScroll> {
  final _tabController = ScrollController();
  final _scrollController = ScrollController();
  List<double> _offsetContentOrigin = [];
  int _indexTab = 0;
  @override
  void initState() {
    _scrollController.addListener(_scrollListener);
    Timer(Duration(milliseconds: 150), () {
      _setOriginOffset();

    });
    super.initState();
  }

  void _scrollListener() {
    if (widget.build != null) {
        int activeTab = 0;
        double offsetTab;
        double widthDevice = MediaQuery
            .of(context)
            .size
            .width;
        double itemSize = widthDevice / 3;
        double offsetStart = widthDevice / 2 - itemSize / 2;

        int indexCheck = _offsetContentOrigin.indexWhere((item) {
          return item - 1 > _scrollController.offset;
        });
        if (indexCheck == -1) {
          activeTab = _offsetContentOrigin.length - 1;
          offsetTab = offsetStart + itemSize * (activeTab - 3);
        } else if (indexCheck > 0) {
          activeTab = indexCheck - 1 > 0 ? indexCheck - 1 : 0;
          offsetTab =
          activeTab > 1 ? offsetStart + itemSize * (activeTab - 2) : 0;
        }
        //print(activeTab);
        if (activeTab != _indexTab && activeTab != -1) {
          setState(() {
            _indexTab = activeTab;
          });
        }
        if (offsetTab != null && activeTab != -1 && widget.build.length > 3) {
          _tabController.jumpTo(offsetTab);
        }

    }
  }

  ///Set Origin Offset default when render success
  void _setOriginOffset() {
    if (widget.build != null && _offsetContentOrigin.isEmpty) {
      setState(() {
        _offsetContentOrigin = widget.build.map((item) {
          final RenderBox box =
          item.keyContentItem.currentContext.findRenderObject();
          final position = box.localToGlobal(Offset.zero);
          return position.dy +
              _scrollController.offset -
              tabHeight -
              AppBar().preferredSize.height -
              MediaQuery.of(context).padding.top;
        }).toList();
      });
    }
  }

  ///On Change tab jumpTo offset
  ///Scroll controller will handle setState active tab
  void _onChangeTab(int index) {
    if (_offsetContentOrigin.isNotEmpty) {
      _scrollController.animateTo(
        _offsetContentOrigin[index] + 1,
        duration: Duration(milliseconds: 500),
        curve: Curves.easeInOutCubic,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    //print(widget.build.length);
    if(widget.build != null && widget.build.length > 0){
    return CupertinoScrollbar(
      child: CustomScrollView(
        physics: BouncingScrollPhysics(),
        controller: _scrollController,
        slivers: <Widget>[
          SliverPersistentHeader(
            pinned: true,
            floating: false,
            delegate: TabBar(
              height: tabHeight,
              tabController: _tabController,
              onIndexChanged: _onChangeTab,
              indexTab: _indexTab,
              tab: widget.build,
            ),
          ),
          SliverToBoxAdapter(
            child: SafeArea(
              top: false,
              child: Container(
                margin: EdgeInsets.only(bottom: 100),
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: List.generate(widget.build.length, (index){
                    //print(widget.build[index].key);
                    return Container(
                      key: widget.build[index].keyContentItem,
                      child: widget.build[index].body != null ? widget.build[index].body : Container(color: Colors.transparent,) ,
                    );
                  }),
                ),
              ),
            ),
          )
        ],
      ),
    );
    } else {
      return Container(color: Colors.transparent);
    }
  }
}


class TabBar extends SliverPersistentHeaderDelegate {
  final double height;
  final ScrollController tabController;
  final ValueChanged<int> onIndexChanged;
  final List<FormTabScrollModel> tab;
  final int indexTab;

  TabBar({
    this.height,
    this.tabController,
    this.onIndexChanged,
    this.tab,
    this.indexTab,
  });

  @override
  Widget build(context, shrinkOffset, overlapsContent) {
    if (tab == null) {
      return SafeArea(
        top: false,
        bottom: false,
        child: Shimmer.fromColors(
          baseColor: Theme.of(context).hoverColor,
          highlightColor: Theme.of(context).highlightColor,
          enabled: true,
          child: Container(
            color: Colors.white,
            margin: EdgeInsets.only(left: 20, right: 20),
          ),
        ),
      );
    }

    return SafeArea(
      top: false,
      bottom: false,
      child: Container(
        color: Colors.white,
        child: ListView.builder(
          controller: tabController,
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            final item = tab[index];
            final active = indexTab == index;
            return InkWell(
              onTap: () {
                onIndexChanged(index);
              },
              child: Container(
                key: item.keyTabItem,
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width / 3,
                padding: EdgeInsets.only(top: 3, left: 8, right: 8),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: active
                          ? Theme.of(context).accentColor
                          : Theme.of(context).dividerColor,
                      width: 3,
                    ),
                  ),
                ),
                child: Text(
                  item.title,
                  style: Theme.of(context).textTheme.subtitle.copyWith(
                    fontWeight: active ? FontWeight.w600 : null,
                  ),
                ),
              ),
            );
          },
          itemCount: tab.length,
        ),
      ),
    );
  }

  @override
  double get maxExtent => height;

  @override
  double get minExtent => height;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}