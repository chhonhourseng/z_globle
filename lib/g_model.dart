export 'model/DefaultModel.dart';
export 'model/bottomNavigationBarModel.dart';
export 'model/CoImageGalleryModel.dart';
export 'model/rangeDate.dart';
export 'model/LanguageModel.dart';

enum CoDateType{dayMonthYear, monthYear, year}

class CoSR{
  static const token ='coToken';
  static const userSessionVersion ='userSessionVersion';
  static const appLanguage ='coAppLanguage';
  static const AppLanguageVersion ='versionAppLanguage';
  static const appLanguageCurrent ='coAppLanguageCurrent';
}